package com.sayplz.domain

import android.content.Context
import com.sayplz.domain.account.AccountInteractor
import com.sayplz.domain.blockchain.BlockchainInteractor

interface DataInteractor {
    val account: AccountInteractor
    val blockchain: BlockchainInteractor

    fun configure(context: Context)
}
