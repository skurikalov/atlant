package com.sayplz.domain.account.model

data class ProfileEntity(
    val accountId: String,
    val avatarUrl: String,
    val email: String,
    val firstName: String,
    val gender: String?,
    val joinedAt: String,
    val kycVerified: Boolean,
    val langsSpokenNames: List<String>,
    val lastName: String,
    val location: String,
    val phoneCountry: String?,
    val phoneNumber: String?,
    val profileId: String,
    val profileType: String
)
