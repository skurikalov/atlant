package com.sayplz.domain.account.model

data class UserEntity(
    val account: AccountEntity,
    val profiles: List<ProfileEntity>
)