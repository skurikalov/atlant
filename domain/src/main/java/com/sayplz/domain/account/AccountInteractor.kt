package com.sayplz.domain.account

import com.sayplz.domain.account.model.UserEntity


interface AccountInteractor {
    suspend fun login(email: String, password: String): Result<Unit>
    suspend fun refreshToken(): Result<Unit>
    suspend fun logout(): Result<Unit>
    suspend fun getUser(): Result<UserEntity>
    fun isLogged(): Boolean
}
