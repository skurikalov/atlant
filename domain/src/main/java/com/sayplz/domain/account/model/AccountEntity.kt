package com.sayplz.domain.account.model

data class AccountEntity(
    val accountId: String,
    val accountType: String,
    val createdAt: String,
    val email: String,
    val emailVerified: Boolean,
    val password: String,
    val phone: String
)