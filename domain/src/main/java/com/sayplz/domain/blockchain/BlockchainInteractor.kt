package com.sayplz.domain.blockchain

import com.sayplz.domain.blockchain.model.TransactionEntity
import kotlinx.coroutines.flow.Flow


interface BlockchainInteractor {
    fun start(): Flow<TransactionEntity>
    fun stop()
}
