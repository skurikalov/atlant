package com.sayplz.domain.blockchain.model

import java.math.BigDecimal
import java.util.*

data class TransactionEntity(
    val op: String,
    val hash: String,
    val time: Date,
    val total: BigDecimal
)
