package com.sayplz.data.transport.model

import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

internal class ResponseResultAdapter<T : Any>(private val type: Type) : CallAdapter<T, Call<Result<T>>> {
    override fun responseType(): Type {
        return type
    }

    override fun adapt(call: Call<T>): Call<Result<T>> {
        return ResponseResultCall(call)
    }
}
