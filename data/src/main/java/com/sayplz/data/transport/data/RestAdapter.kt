package com.sayplz.data.transport.data


import com.facebook.stetho.okhttp3.StethoInterceptor
import com.sayplz.data.common.Constants.KARTA_API_URL
import com.sayplz.data.common.GsonFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit


internal class RestAdapter(private val getAccessToken: () -> String?) {
    fun createRestAdapter(): Retrofit {
        val httpClient = createOkHttpClient()
        return Retrofit.Builder()
            .baseUrl(KARTA_API_URL)
            .addConverterFactory(GsonFactory.getConverterFactory())
            .addCallAdapterFactory(ResultCallAdapterFactory())
            .client(httpClient)
            .build()
    }

    private fun createOkHttpClient() = OkHttpClient.Builder()
        .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
        .authenticator(RefreshTokenAuthenticator(getAccessToken))
        .addInterceptor(createExtraHeadersInterceptor())
        .addNetworkInterceptor(StethoInterceptor())
        .build()

    private fun createExtraHeadersInterceptor(): Interceptor {
        return Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
                .header(HEADER_ACCEPT, "application/json")

            val token = getAccessToken()
            if (token.isNullOrEmpty()) {
                requestBuilder.removeHeader(HEADER_AUTH)
            } else {
                requestBuilder.addHeader(HEADER_AUTH, token)
            }

            return@Interceptor chain.proceed(requestBuilder.build())
        }
    }

    companion object {
        const val HEADER_AUTH = "Authorization"
        private const val TIMEOUT = 30L
        private const val HEADER_ACCEPT = "Accept"
    }
}