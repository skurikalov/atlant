package com.sayplz.data.transport

import com.sayplz.data.transport.controller.TransportController

internal interface TransportInteractor {
    fun <T> provideService(service: Class<T>): T
}

internal object TransportRepository : TransportInteractor {
    private val transport = TransportController()

    override fun <T> provideService(service: Class<T>): T {
        return transport.provideService(service)
    }
}
