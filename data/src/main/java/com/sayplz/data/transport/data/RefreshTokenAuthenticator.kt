package com.sayplz.data.transport.data

import com.sayplz.data.DataModule
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

internal class RefreshTokenAuthenticator(private val getAccessToken: () -> String?) :
    Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val url = response.networkResponse()?.request()?.url()?.toString()
        if (url?.endsWith("auth") != true) {
            runBlocking { DataModule.account.refreshToken() }
            return response.request().newBuilder()
                .header(RestAdapter.HEADER_AUTH, getAccessToken() ?: "")
                .build()
        } else {
            return null
        }
    }
}

