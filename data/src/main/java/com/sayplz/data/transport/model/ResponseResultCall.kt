package com.sayplz.data.transport.model

import retrofit2.Call
import retrofit2.Callback

internal class ResponseResultCall<T : Any>(proxy: Call<T>) : ResultCallDelegate<T, Result<T>>(proxy) {

    override fun enqueueImpl(callback: Callback<Result<T>>) {
        proxy.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: retrofit2.Response<T>) {
                callback.onResponse(this@ResponseResultCall, retrofit2.Response.success(Result.create(response)))
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                callback.onResponse(this@ResponseResultCall, retrofit2.Response.success(Result.failure(Exception(t))))
            }
        })
    }

    override fun cloneImpl(): ResponseResultCall<T> {
        return ResponseResultCall(proxy.clone())
    }
}

private fun <T> Result.Companion.create(response: retrofit2.Response<T>): Result<T>? {
    return if (response.isSuccessful) {
        response.body()?.let { success(it) }
    } else {
        val msg = response.errorBody()?.string()
        failure(Exception(msg))
    }
}
