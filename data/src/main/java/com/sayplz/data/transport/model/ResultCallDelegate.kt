package com.sayplz.data.transport.model

import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback

internal abstract class ResultCallDelegate<TIn, TOut>(protected val proxy: Call<TIn>) : Call<TOut> {
    final override fun enqueue(callback: Callback<TOut>) = enqueueImpl(callback)
    final override fun clone(): Call<TOut> = cloneImpl()

    override fun execute(): retrofit2.Response<TOut> = throw NotImplementedError()
    override fun cancel() = proxy.cancel()
    override fun request(): Request = proxy.request()
    override fun isExecuted() = proxy.isExecuted
    override fun isCanceled() = proxy.isCanceled

    abstract fun enqueueImpl(callback: Callback<TOut>)
    abstract fun cloneImpl(): Call<TOut>
}
