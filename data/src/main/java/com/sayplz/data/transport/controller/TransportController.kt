package com.sayplz.data.transport.controller


import com.sayplz.data.Storage
import com.sayplz.data.transport.data.RestAdapter


internal class TransportController {
    private var restAdapter = RestAdapter { getAccessToken() }

    fun <T> provideService(service: Class<T>): T {
        return restAdapter.createRestAdapter().create(service)
    }

    private fun getAccessToken(): String? {
        return getCachedAccessToken() ?: getStoredAccessToken()
    }

    private fun getCachedAccessToken() = Storage.memory
        .get(ACCESS_TOKEN_STORAGE_KEY, String::class.java)
        .getOrNull()

    private fun getStoredAccessToken() = Storage.local
        .get(ACCESS_TOKEN_STORAGE_KEY, String::class.java)
        .getOrNull()

    companion object {
        const val ACCESS_TOKEN_STORAGE_KEY = "access_token"
    }
}
