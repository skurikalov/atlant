package com.sayplz.data.transport.data

import com.sayplz.data.transport.model.ResponseResultAdapter
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

internal class ResultCallAdapterFactory : CallAdapter.Factory() {
    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit
    ): ResponseResultAdapter<Any>? {
        return when (Call::class.java) {
            getRawType(returnType) -> responseRawTypeResultAdapter(returnType)
            else -> null
        }
    }

    private fun responseRawTypeResultAdapter(returnType: Type): ResponseResultAdapter<Any>? {
        val callType = getParameterUpperBound(0, returnType as ParameterizedType)
        return if (getRawType(callType) == Result::class.java) {
            require(callType is ParameterizedType) { "resource must be paramterized" }
            val resultType = getParameterUpperBound(0, callType)
            ResponseResultAdapter(resultType)
        } else {
            null
        }
    }
}
