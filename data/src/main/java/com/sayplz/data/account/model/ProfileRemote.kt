package com.sayplz.data.account.model

import com.google.gson.annotations.SerializedName
import com.sayplz.domain.account.model.ProfileEntity

internal data class ProfileRemote(
    @SerializedName("account_id") val accountId: String,
    @SerializedName("avatar_url") val avatarUrl: String,
    @SerializedName("email") val email: String,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("gender") val gender: String?,
    @SerializedName("joined_at") val joinedAt: String,
    @SerializedName("kyc_verified") val kycVerified: Boolean,
    @SerializedName("langs_spoken_names") val langsSpokenNames: List<String>,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("location") val location: String,
    @SerializedName("phone_country") val phoneCountry: String?,
    @SerializedName("phone_number") val phoneNumber: String?,
    @SerializedName("profile_id") val profileId: String,
    @SerializedName("profile_type") val profileType: String
) {
    fun toEntity(): ProfileEntity {
        return ProfileEntity(
            this.accountId,
            this.avatarUrl,
            this.email,
            this.firstName,
            this.gender,
            this.joinedAt,
            this.kycVerified,
            this.langsSpokenNames,
            this.lastName,
            this.location,
            this.phoneCountry,
            this.phoneNumber,
            this.profileId,
            this.profileType
        )
    }
}
