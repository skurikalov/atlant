package com.sayplz.data.account.model

internal data class SessionLocal(
    val exp: Long,
    val accountId: String,
    val sessionId: String,
    val accountType: String
)
