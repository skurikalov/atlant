package com.sayplz.data.account.model

internal data class InfoRemote(
    val account: AccountRemote,
    val profiles: List<ProfileRemote>
)