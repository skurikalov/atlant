package com.sayplz.data.account.model

import com.sayplz.domain.account.model.UserEntity

internal data class UserRemote(
    val info: InfoRemote
) {
    fun toEntity(): UserEntity {
        return UserEntity(
            this.info.account.toEntity(),
            this.info.profiles.map { it.toEntity() }
        )
    }
}
