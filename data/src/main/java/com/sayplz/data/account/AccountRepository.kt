package com.sayplz.data.account

import com.sayplz.data.account.controller.AccountController
import com.sayplz.domain.account.AccountInteractor
import com.sayplz.domain.account.model.UserEntity

internal class AccountRepository : AccountInteractor {
    private val controller = AccountController()

    override suspend fun login(email: String, password: String): Result<Unit> {
        return controller.login(email, password)
    }

    override suspend fun refreshToken(): Result<Unit> {
        return controller.refreshToken()
    }

    override suspend fun logout(): Result<Unit> {
        return controller.logout()
    }

    override suspend fun getUser(): Result<UserEntity> {
        return controller.getUser()
    }

    override fun isLogged(): Boolean {
        return controller.isLogged()
    }
}
