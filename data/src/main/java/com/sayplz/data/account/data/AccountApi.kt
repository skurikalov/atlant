package com.sayplz.data.account.data

import com.sayplz.data.account.model.AuthRequestRemote
import com.sayplz.data.account.model.AuthResponseRemote
import com.sayplz.data.account.model.LogoutRequestRemote
import com.sayplz.data.account.model.UserRemote
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

internal interface AccountApi {
    @POST("accounts/auth")
    suspend fun login(@Body request: AuthRequestRemote): Result<AuthResponseRemote>

    @POST("accounts/sessions/refresh")
    suspend fun refreshToken(): Result<AuthResponseRemote>

    @POST("accounts/sessions/end")
    suspend fun logout(@Body request: LogoutRequestRemote): Result<Unit>

    @GET("accounts/current")
    suspend fun getUser(): Result<UserRemote>
}