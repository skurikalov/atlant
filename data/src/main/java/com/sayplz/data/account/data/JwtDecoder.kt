package com.sayplz.data.account.data

import android.util.Base64
import com.sayplz.data.common.GsonFactory

internal object JwtDecoder {
    @Throws
    fun <T> decode(accessToken: String, clazz: Class<T>): T {
        val token = accessToken.split(".")[1]
        val jwtRaw = String(Base64.decode(token, Base64.DEFAULT))
        return GsonFactory.getGson().fromJson(jwtRaw, clazz)
    }
}
