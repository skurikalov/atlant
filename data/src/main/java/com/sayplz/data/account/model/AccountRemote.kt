package com.sayplz.data.account.model

import com.google.gson.annotations.SerializedName
import com.sayplz.domain.account.model.AccountEntity

internal data class AccountRemote(
    @SerializedName("account_id") val accountId: String,
    @SerializedName("account_type") val accountType: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("email") val email: String,
    @SerializedName("email_verified") val emailVerified: Boolean,
    @SerializedName("password") val password: String,
    @SerializedName("phone") val phone: String
) {
    fun toEntity(): AccountEntity {
        return AccountEntity(
            this.accountId,
            this.accountType,
            this.createdAt,
            this.email,
            this.emailVerified,
            this.password,
            this.phone
        )
    }
}
