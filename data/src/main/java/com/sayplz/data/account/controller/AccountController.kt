package com.sayplz.data.account.controller

import com.sayplz.data.Storage
import com.sayplz.data.Transport
import com.sayplz.data.account.data.AccountApi
import com.sayplz.data.account.data.JwtDecoder
import com.sayplz.data.account.model.*
import com.sayplz.data.transport.controller.TransportController.Companion.ACCESS_TOKEN_STORAGE_KEY
import com.sayplz.domain.account.model.UserEntity

internal class AccountController {
    private val api = Transport.provideService(AccountApi::class.java)

    suspend fun login(email: String, password: String): Result<Unit> {
        return api.login(AuthRequestRemote(email, password))
            .mapCatching(::saveSession)
    }

    suspend fun refreshToken(): Result<Unit> {
        return api.refreshToken()
            .mapCatching(::saveSession)
    }

    suspend fun logout(): Result<Unit> {
        val sessionId = getSession()?.sessionId
        return if (sessionId != null) {
            api.logout(LogoutRequestRemote(sessionId))
                .onSuccess { clearSession() }
        } else {
            Result.failure(IllegalArgumentException("Session not found"))
        }
    }

    suspend fun getUser(): Result<UserEntity> {
        return api.getUser()
            .mapCatching { it.toEntity() }
    }

    fun isLogged(): Boolean {
        val token = Storage.local.get(ACCESS_TOKEN_STORAGE_KEY, String::class.java).getOrNull()
        return token != null
    }

    private fun clearSession() {
        Storage.cleanAll()
    }

    private fun getSession(): SessionLocal? {
        return Storage.local.get(SESSION_STORAGE_KEY, SessionLocal::class.java).getOrNull()
    }

    @Throws
    private fun saveSession(response: AuthResponseRemote) {
        val session = extractSession(response.token)
        Storage.memory.store(ACCESS_TOKEN_STORAGE_KEY, response.token).getOrThrow()
        Storage.local.store(ACCESS_TOKEN_STORAGE_KEY, response.token).getOrThrow()
        Storage.local.store(SESSION_STORAGE_KEY, session).getOrThrow()
    }

    @Throws
    private fun extractSession(token: String): SessionLocal {
        val remote = JwtDecoder.decode(token, SessionRemote::class.java)
        return remote.toLocal()
    }

    companion object {
        private const val SESSION_STORAGE_KEY = "session_key"
    }
}
