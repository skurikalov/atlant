package com.sayplz.data.account.model

internal data class AuthRequestRemote(
    val email: String,
    val password: String
)
