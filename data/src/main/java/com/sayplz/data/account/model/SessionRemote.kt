package com.sayplz.data.account.model

import com.google.gson.annotations.SerializedName

internal data class SessionRemote(
    val exp: Long,
    @SerializedName("account_id") val accountId: String,
    @SerializedName("session_id") val sessionId: String,
    @SerializedName("account_type") val accountType: String
) {
    fun toLocal(): SessionLocal {
        return SessionLocal(
            this.exp,
            this.accountId,
            this.sessionId,
            this.accountType
        )
    }
}
