package com.sayplz.data.account.model

internal data class AuthResponseRemote(
    val token: String
)
