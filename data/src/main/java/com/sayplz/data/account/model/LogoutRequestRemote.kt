package com.sayplz.data.account.model

import com.google.gson.annotations.SerializedName

internal data class LogoutRequestRemote(
    @SerializedName("session_id") val sessionId: String
)
