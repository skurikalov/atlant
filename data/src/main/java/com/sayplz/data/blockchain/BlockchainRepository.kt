package com.sayplz.data.blockchain

import com.sayplz.data.blockchain.controller.BlockchainController
import com.sayplz.domain.blockchain.BlockchainInteractor
import com.sayplz.domain.blockchain.model.TransactionEntity
import kotlinx.coroutines.flow.Flow

internal class BlockchainRepository : BlockchainInteractor {
    private val controller = BlockchainController()

    override fun start(): Flow<TransactionEntity> {
        return controller.start()
    }

    override fun stop() {
        controller.stop()
    }
}
