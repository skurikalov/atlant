package com.sayplz.data.blockchain.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

internal data class TransactionPrevOutRemote(
    val addr: String,
    val n: Int,
    val script: String,
    val spent: Boolean,
    @SerializedName("tx_index") val txIndex: Int,
    val type: Int,
    val value: BigDecimal
)