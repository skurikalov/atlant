package com.sayplz.data.blockchain.model

internal data class TransactionRemote(
    val op: String,
    val x: TransactionXRemote
)
