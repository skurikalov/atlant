package com.sayplz.data.blockchain.model

import com.google.gson.annotations.SerializedName

internal data class TransactionXRemote(
    val hash: String,
    val inputs: List<TransactionInputRemote>,
    @SerializedName("out") val out: List<TransactionOutRemote>,
    @SerializedName("lock_time") val lockTime: Int,
    @SerializedName("relayed_by") val relayedBy: String,
    @SerializedName("tx_index") val txIndex: Int,
    @SerializedName("vin_sz") val vinSz: Int,
    @SerializedName("vout_sz") val voutSz: Int,
    val size: Int,
    val time: Long,
    val ver: Int
)
