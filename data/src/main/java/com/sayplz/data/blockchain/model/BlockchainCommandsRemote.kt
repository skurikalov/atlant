package com.sayplz.data.blockchain.model


internal data class BlockchainCommandsRemote(
    val op: BlockchainOperations
)
