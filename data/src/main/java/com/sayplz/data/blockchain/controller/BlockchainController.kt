package com.sayplz.data.blockchain.controller

import com.sayplz.data.blockchain.data.BlockchainWebSocketAdapter
import com.sayplz.data.blockchain.model.BlockchainCommandsRemote
import com.sayplz.data.blockchain.model.BlockchainOperations
import com.sayplz.data.blockchain.model.TransactionRemote
import com.sayplz.data.common.GsonFactory
import com.sayplz.domain.blockchain.model.TransactionEntity
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.math.BigDecimal
import java.util.*


internal class BlockchainController {
    private val adapter = BlockchainWebSocketAdapter(getWebSocketListener())
    private lateinit var transactions: Channel<TransactionEntity>
    private val gson = GsonFactory.getGson()

    fun start(): Flow<TransactionEntity> {
        transactions = Channel(capacity = Channel.CONFLATED)
        adapter.start()
        return transactions.consumeAsFlow()
    }

    fun stop() {
        transactions.cancel()
        adapter.stop()
    }

    private fun getWebSocketListener() = object : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response) {
            super.onOpen(webSocket, response)
            webSocket.send(getOperation())
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            super.onMessage(webSocket, text)
            sendTransaction(text)
        }
    }

    private fun TransactionRemote.toEntity(): TransactionEntity {
        val total = this.x.out
            .map { it.value }
            .fold(BigDecimal.ZERO, BigDecimal::add)
            .divide(BigDecimal(100000000))

        return TransactionEntity(
            this.op,
            this.x.hash,
            Date(this.x.time),
            total
        )
    }

    private fun getOperation(): String {
        val operation = BlockchainCommandsRemote(BlockchainOperations.UNCONFIRMED_SUB)
        return gson.toJson(operation)
    }

    private fun sendTransaction(text: String) {
        val transaction = gson.fromJson(text, TransactionRemote::class.java)
        transactions.offer(transaction.toEntity())
    }
}
