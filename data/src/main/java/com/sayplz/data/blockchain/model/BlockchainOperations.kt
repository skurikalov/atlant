package com.sayplz.data.blockchain.model

import com.google.gson.annotations.SerializedName

internal enum class BlockchainOperations {
    @SerializedName("ping")
    PING,
    @SerializedName("unconfirmed_sub")
    UNCONFIRMED_SUB,
    @SerializedName("unconfirmed_unsub")
    UNCONFIRMED_UNSUB
}
