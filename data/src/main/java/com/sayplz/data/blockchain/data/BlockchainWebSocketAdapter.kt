package com.sayplz.data.blockchain.data

import com.sayplz.data.common.Constants.BLOCKCHAIN_URL
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.util.concurrent.TimeUnit


internal class BlockchainWebSocketAdapter(val listener: WebSocketListener) {
    private var webSocket: WebSocket? = null

    fun start() {
        webSocket = getOkHttpClient().newWebSocket(getRequest(), listener)
        getOkHttpClient().dispatcher().executorService().shutdown()
    }

    fun stop() {
        webSocket?.close(NORMAL_CLOSURE_STATUS, null)
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .pingInterval(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .build()
    }

    private fun getRequest(): Request {
        return Request.Builder().url(BLOCKCHAIN_URL).build()
    }

    companion object {
        private const val NORMAL_CLOSURE_STATUS = 1000
        private const val TIMEOUT_SECONDS = 20L
    }
}
