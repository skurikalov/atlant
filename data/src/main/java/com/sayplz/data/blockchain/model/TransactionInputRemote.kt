package com.sayplz.data.blockchain.model

import com.google.gson.annotations.SerializedName

internal data class TransactionInputRemote(
    @SerializedName("prev_out") val prevOut: TransactionPrevOutRemote,
    val script: String,
    val sequence: Long
)
