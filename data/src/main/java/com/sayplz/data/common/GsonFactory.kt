package com.sayplz.data.common

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.converter.gson.GsonConverterFactory

internal object GsonFactory {
    fun getConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(getGson())
    }

    fun getGson(): Gson {
        return GsonBuilder().create()
    }
}
