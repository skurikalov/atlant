package com.sayplz.data.common

internal object Constants {
    const val BLOCKCHAIN_URL = "wss://ws.blockchain.info/inv"
    const val KARTA_API_URL = "https://api.dev.karta.com"
}
