package com.sayplz.data.storage


import android.content.Context
import com.sayplz.data.storage.data.PreferenceAgent
import com.smartsatu.easyshopkit.storage.controller.CleanStorageController
import com.smartsatu.easyshopkit.storage.data.MemoryAgent


internal interface StorageInterface {
    fun configure(context: Context)
    fun clean(): Result<Unit>
    fun <T> get(key: String, clazz: Class<T>, default: T? = null): Result<T>
    fun store(key: String, data: Any): Result<Unit>
    fun remove(key: String): Result<Unit>
}

internal object StorageRepository {
    val memory: StorageInterface by lazy {
        MemoryAgent()
    }

    val local: StorageInterface by lazy {
        PreferenceAgent()
    }

    fun configure(context: Context) {
        local.configure(context)
    }

    fun cleanAll(): Result<Unit> {
        return CleanStorageController().cleanAll()
    }
}
