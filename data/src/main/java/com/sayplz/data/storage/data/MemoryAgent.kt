package com.smartsatu.easyshopkit.storage.data

import android.content.Context
import com.sayplz.data.storage.StorageInterface


internal class MemoryAgent : StorageInterface {
    private val cache = HashMap<String, Any>()

    override fun configure(context: Context) {
        throw NotImplementedError()
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(key: String, clazz: Class<T>, default: T?): Result<T> {
        return runCatching { cache[key] as T }
    }

    override fun store(key: String, data: Any): Result<Unit> {
        return runCatching { cache[key] = data }
    }

    override fun remove(key: String): Result<Unit> {
        return runCatching { cache.remove(key) }.map { }
    }

    override fun clean(): Result<Unit> {
        return runCatching { cache.clear() }
    }
}
