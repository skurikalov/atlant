package com.sayplz.data.storage.data

import android.content.Context
import android.content.SharedPreferences
import com.sayplz.data.storage.StorageInterface
import com.sayplz.data.common.GsonFactory

internal class PreferenceAgent : StorageInterface {
    companion object {
        private const val APP_PREFERENCES_NAME = "ATLANT_APP_PREFERENCES"
    }

    private lateinit var preferences: SharedPreferences
    private val gson = GsonFactory.getGson()

    override fun configure(context: Context) {
        preferences = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    override fun clean(): Result<Unit> {
        return runCatching {
            preferences.edit().clear().apply()
        }
    }

    override fun <T> get(key: String, clazz: Class<T>, default: T?): Result<T> {
        return runCatching {
            val json = preferences.getString(key, "")
            val value = gson.fromJson(json, clazz) ?: default
            return@runCatching value ?: throw IllegalArgumentException("Cannot deserialize data")
        }
    }

    override fun store(key: String, data: Any): Result<Unit> {
        return runCatching {
            val json = gson.toJson(data)
            preferences.edit().putString(key, json).apply()
        }
    }

    override fun remove(key: String): Result<Unit> {
        return runCatching {
            preferences.edit().remove(key).apply()
        }
    }
}
