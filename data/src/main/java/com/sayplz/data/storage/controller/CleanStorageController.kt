package com.smartsatu.easyshopkit.storage.controller

import com.sayplz.data.storage.StorageRepository

class CleanStorageController {
    private val storages = listOf(
        StorageRepository.local,
        StorageRepository.memory
    )

    fun cleanAll(): Result<Unit> {
        return runCatching {
            storages.forEach { it.clean() }
        }
    }
}
