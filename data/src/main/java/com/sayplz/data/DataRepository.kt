package com.sayplz.data

import android.content.Context
import com.facebook.stetho.Stetho
import com.sayplz.data.account.AccountRepository
import com.sayplz.data.blockchain.BlockchainRepository
import com.sayplz.domain.DataInteractor
import com.sayplz.domain.account.AccountInteractor
import com.sayplz.domain.blockchain.BlockchainInteractor

object DataRepository : DataInteractor {
    override val account: AccountInteractor by lazy {
        AccountRepository()
    }

    override val blockchain: BlockchainInteractor by lazy {
        BlockchainRepository()
    }

    override fun configure(context: Context) {
        Stetho.initializeWithDefaults(context)
        Storage.configure(context)
    }
}
