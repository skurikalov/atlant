package com.sayplz.data

import com.sayplz.data.storage.StorageRepository
import com.sayplz.data.transport.TransportRepository

internal typealias DataModule = DataRepository
internal typealias Storage = StorageRepository
internal typealias Transport = TransportRepository
