# Atlant

### Некоторые заметки
* Для бэкгранд задач использовал kotlin coroutines.
* В проекте помимо использования clean arch, используется модульный подход, легко расширять приложение модулями.
* В data layer все классы internal, кроме DataRepository (используется как фасад наружу), чтобы ограничить вызов напрямую из data layer.
* Не использовал DI:
# 
	Для поддержки inversion of control использовал возможность kotlin объявления переменных в интерфейсе. Пример из проекта:
	В domain layer: 
		interface DataInteractor {
			val account: AccountInteractor
			val blockchain: BlockchainInteractor
		}
	В data layer:
		class DataRepository: DataInteractor {
			val account: AccountInteractor = AccountRepository()
			val blockchain: BlockchainInteractor = BlockchainRepository()
		}
	Presentation layer:
		В итоге Presentation layer ничего не знает как и что мы реализовали в data layer, достаточно знать, что мы можем использовать из интерфейсов в domain layer.

### P.S.
На этом все. Пишу проекты с использованием Clean Architecture около 3х лет начиная с проекта для городского транспорта для Новой Зеландии.

Использовал разные техники и подходы, везде есть свои минусы и плюсы, но сам шаблон проектирования Clean Architecture считаю лучшим на данный момент.

Спасибо, что дочитали.
