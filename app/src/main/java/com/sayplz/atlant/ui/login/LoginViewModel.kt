package com.sayplz.atlant.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sayplz.atlant.DataModule
import com.sayplz.atlant.common.uiResult

class LoginViewModel : ViewModel() {
    fun login(email: String, password: String) = viewModelScope.uiResult {
        DataModule.account.login(email, password)
    }
}
