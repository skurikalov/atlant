package com.sayplz.atlant.ui.content

import androidx.lifecycle.ViewModel
import com.sayplz.atlant.DataModule

class ContentViewModel : ViewModel() {
    fun isLogged(): Boolean {
        return DataModule.account.isLogged()
    }
}
