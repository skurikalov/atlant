package com.sayplz.atlant.ui.profile

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sayplz.atlant.BR
import com.sayplz.atlant.R
import com.sayplz.atlant.common.bind
import com.sayplz.domain.blockchain.model.TransactionEntity


class TransactionAdapter : ListAdapter<TransactionEntity, RecyclerView.ViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(parent.bind(R.layout.transaction_item))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ViewHolder) {
            this.bind(getItem(position))
        }
    }

    class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TransactionEntity) {
            binding.setVariable(BR.item, item)
            binding.executePendingBindings()
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<TransactionEntity>() {
            override fun areItemsTheSame(old: TransactionEntity, new: TransactionEntity) =
                old.hash == new.hash

            override fun areContentsTheSame(old: TransactionEntity, new: TransactionEntity) =
                old == new
        }
    }
}
