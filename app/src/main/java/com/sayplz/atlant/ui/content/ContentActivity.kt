package com.sayplz.atlant.ui.content

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.sayplz.atlant.R
import com.sayplz.atlant.ui.login.LoginFragment
import com.sayplz.atlant.ui.profile.ProfileFragment

class ContentActivity : AppCompatActivity(), NavigationInterface {
    private val viewModel by viewModels<ContentViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        if (savedInstanceState == null) {
            route()
        }
    }

    private fun route() {
        if (viewModel.isLogged()) {
            navigateToUser()
        } else {
            navigateToLogin()
        }
    }

    override fun navigateToLogin() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, LoginFragment())
            .disallowAddToBackStack()
            .commitNow()
    }

    override fun navigateToUser() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ProfileFragment())
            .disallowAddToBackStack()
            .commitNow()
    }
}
