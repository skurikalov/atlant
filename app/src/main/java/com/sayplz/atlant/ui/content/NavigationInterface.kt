package com.sayplz.atlant.ui.content

interface NavigationInterface {
    fun navigateToLogin()
    fun navigateToUser()
}
