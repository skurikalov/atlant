package com.sayplz.atlant.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.sayplz.atlant.R
import com.sayplz.atlant.common.UiResult
import com.sayplz.atlant.common.bind
import com.sayplz.atlant.common.findNavController
import com.sayplz.atlant.common.showError
import com.sayplz.atlant.databinding.FragmentProfileBinding
import com.sayplz.domain.account.model.UserEntity


class ProfileFragment : Fragment() {
    private lateinit var binding: FragmentProfileBinding
    private val viewModel by viewModels<ProfileViewModel>()
    private val adapter by lazy { TransactionAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = container.bind(R.layout.fragment_profile, inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        getProfile()
        setupRefreshProfile()
        setupLogout()
        setupBlockchainTransactions()
    }

    private fun setupViewModel() {
        binding.viewModel = viewModel
        binding.executePendingBindings()
        binding.lifecycleOwner = viewLifecycleOwner
        lifecycle.addObserver(viewModel)
    }

    private fun setupRefreshProfile() {
        binding.refresh.setOnRefreshListener {
            getProfile()
        }
    }

    private fun setupLogout() {
        binding.logout.setOnClickListener {
            logout()
        }
    }

    private fun logout() {
        viewModel.logout().observe(viewLifecycleOwner, Observer { result ->
            binding.logout.isEnabled = !result.inProgress
            when (result) {
                is UiResult.Success -> findNavController().navigateToLogin()
                is UiResult.Failure -> showError(R.string.logout_error)
            }
        })
    }

    private fun getProfile() {
        viewModel.getUser().observe(viewLifecycleOwner, Observer { result ->
            binding.refresh.isRefreshing = result.inProgress
            when (result) {
                is UiResult.Success -> showProfile(result.data)
                is UiResult.Failure -> showError(R.string.get_profile_error)
            }
        })
    }

    private fun showProfile(user: UserEntity) {
        user.profiles.firstOrNull()?.let { profile ->
            binding.profile = profile
            binding.executePendingBindings()
        }
    }

    private fun setupBlockchainTransactions() {
        binding.transactions.layoutManager = LinearLayoutManager(context)
        binding.transactions.adapter = adapter

        viewModel.getTransactions().observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }
}
