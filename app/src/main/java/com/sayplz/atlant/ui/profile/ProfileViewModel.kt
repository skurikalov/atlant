package com.sayplz.atlant.ui.profile

import androidx.lifecycle.*
import com.sayplz.atlant.DataModule
import com.sayplz.atlant.common.uiResult
import com.sayplz.domain.blockchain.model.TransactionEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.math.BigDecimal


class ProfileViewModel : ViewModel(), LifecycleObserver {
    private val transactions = MutableLiveData<List<TransactionEntity>>()
    private val total = MutableLiveData<BigDecimal>().apply { value = BigDecimal.ZERO }
    private val isStartedTransactions = MutableLiveData<Boolean>().apply { value = false }

    fun getTotal(): LiveData<BigDecimal> {
        return total
    }

    fun getUser() = viewModelScope.uiResult {
        DataModule.account.getUser()
    }

    fun logout() = viewModelScope.uiResult {
        DataModule.account.logout()
    }

    fun getTransactions(): LiveData<List<TransactionEntity>> {
        return transactions
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun start() = viewModelScope.launch(Dispatchers.Main) {
        isStartedTransactions.value = true
        DataModule.blockchain.start().collect { transaction ->
            addTransaction(transaction)
            calculateTotal(transaction)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun stop() {
        isStartedTransactions.value = false
        DataModule.blockchain.stop()
    }

    fun clear() {
        transactions.value = emptyList()
        total.value = BigDecimal.ZERO
    }

    fun isStartedTransactions(): LiveData<Boolean> {
        return isStartedTransactions
    }

    private fun addTransaction(transaction: TransactionEntity) {
        val items = transactions.value?.toMutableList() ?: mutableListOf()
        items.add(0, transaction)
        transactions.value = items
    }

    private fun calculateTotal(transaction: TransactionEntity) {
        val newTotal = (total.value ?: BigDecimal.ZERO).plus(transaction.total)
        total.value = newTotal
    }
}
