package com.sayplz.atlant.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.sayplz.atlant.R
import com.sayplz.atlant.common.UiResult
import com.sayplz.atlant.common.bind
import com.sayplz.atlant.common.findNavController
import com.sayplz.atlant.common.showError
import com.sayplz.atlant.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val viewModel by viewModels<LoginViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = container.bind(R.layout.fragment_login, inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLoginButton()
    }

    private fun setupLoginButton() {
        binding.login.setOnClickListener {
            val email = binding.email.text.toString()
            val password = binding.password.text.toString()
            viewModel.login(email, password).observe(viewLifecycleOwner, Observer {
                showLoginResult(it)
            })
        }
    }

    private fun showLoginResult(result: UiResult<Unit>) {
        binding.login.isEnabled = !result.inProgress
        when (result) {
            is UiResult.Success -> {
                clearFocus()
                findNavController().navigateToUser()
            }
            is UiResult.Failure -> showError(R.string.incorrect_auth_data)
        }
    }

    private fun clearFocus() {
        binding.email.clearFocus()
        binding.password.clearFocus()
    }
}
