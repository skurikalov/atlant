package com.sayplz.atlant.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

fun <T> CoroutineScope.uiResult(exec: suspend () -> Result<T>): LiveData<UiResult<T>> {
    val event = MutableLiveData<UiResult<T>>()
    launch(Dispatchers.Main) {
        event.value = UiResult.InProgress()
        event.value = withContext(Dispatchers.IO) { exec() }
            .fold(
                onSuccess = { UiResult.Success(it) },
                onFailure = { UiResult.failure(it) }
            )
    }
    return event
}
