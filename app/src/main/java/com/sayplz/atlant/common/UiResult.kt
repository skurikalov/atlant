package com.sayplz.atlant.common


sealed class UiResult<T>(val inProgress: Boolean) {
    class InProgress<T> : UiResult<T>(true)
    data class Success<T>(val data: T) : UiResult<T>(false)
    data class Failure<T>(val e: Throwable) : UiResult<T>(false)

    companion object {
        fun <T> inProgress(): UiResult<T> = InProgress()
        fun <T> success(data: T): UiResult<T> = Success(data)
        fun <T> failure(t: Throwable): UiResult<T> = Failure(t)
    }
}
