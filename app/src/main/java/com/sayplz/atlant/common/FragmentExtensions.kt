package com.sayplz.atlant.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.pranavpandey.android.dynamic.toasts.DynamicToast
import com.sayplz.atlant.ui.content.NavigationInterface

fun Fragment.findNavController(): NavigationInterface {
    return requireActivity() as NavigationInterface
}

inline fun <reified T : ViewDataBinding> ViewGroup?.bind(
    @LayoutRes layoutRes: Int,
    inflater: LayoutInflater = LayoutInflater.from(this?.context),
    attachToParent: Boolean = false
): T {
    return DataBindingUtil.inflate(inflater, layoutRes, this, attachToParent)
}

fun Fragment.showSuccess(@StringRes stringRes: Int) {
    return DynamicToast.makeSuccess(requireContext(), requireContext().getString(stringRes)).show()
}

fun Fragment.showError(@StringRes stringRes: Int) {
    return DynamicToast.makeError(requireContext(), requireContext().getString(stringRes)).show()
}
