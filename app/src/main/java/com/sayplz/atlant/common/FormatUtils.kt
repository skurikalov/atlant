package com.sayplz.atlant.common

import java.text.SimpleDateFormat
import java.util.*

private val TIME_FORMAT = SimpleDateFormat("HH:mm", Locale.getDefault())

fun asTime(date: Date?): String {
    return date?.let { TIME_FORMAT.format(date) } ?: ""
}
