package com.sayplz.atlant

import android.app.Application
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.pranavpandey.android.dynamic.toasts.DynamicToast


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setupDataModule()
        setupToast()
    }

    private fun setupDataModule() {
        DataModule.configure(applicationContext)
    }

    private fun setupToast() {
        DynamicToast.Config.getInstance()
            .setDefaultBackgroundColor(color(R.color.primary))
            .setDefaultTintColor(color(android.R.color.white))
            .apply()
    }

    private fun color(@ColorRes color: Int): Int {
        return ContextCompat.getColor(applicationContext, color)
    }
}
